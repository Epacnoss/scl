---
title: Techno2
---

Envoyé à: Pierre Gousteau @ 17:38 12-03-21

Cher Pierre,

Dans ta dernière lettre tu m\'as demandé des raisons pourqui j\'aime la
téchnologie, et dans cette lettre je vais t\'en parler.

Première, ma facon (w/ cedilla) préférée pour rencontrer mes amis est en
ligne à Discord car je peux leur parler même dans cette confinement. En
plus, il ne faut pas aller chez eux. Aussi, j\'aime utiliser mon
ordinateur portable pour mes affaires de l\'école puisque je peux avoir
tout mon travail dans une place et je peux travailler partout. Si je
n\'avais pas de l\'Internet pour un mois, je téléchargerait beaucoup de
livres pour mon Kindle, et en plus je pourrais jouer du bassoon ou
j\'irais pour faire de l\'éxercise ou des sports. Le probleme est les
personnes, mais c\'est aussi l\'advantage - quelques personnes sont très
mauvaises, mais plus personnes sont bien sympa. En plus, C\'est très
facile de trouver l\'information pour les nouvelles choses.

J\'ai hâte de ton opinion sur l\'Internet, à bientôt!

Jack
