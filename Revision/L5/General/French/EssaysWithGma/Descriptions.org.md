---
title: Descriptions Essay
---

Je vais commencer par vous parler de moi-même et mes amis.

Je suis né à Londres en deux-mille-six, et maintenant j\'habite à Putney
(à Londres). Je vais à l\'école à Wimbledon et je suis en Troisième.
J\'étudie beaucoup de matières, mais ma matière préférée est la
technologie. Je l\'aime parce que ma prof est très sympa. Pour ma
personnalité, j\'aime penser que je suis sympa et drôle. Je ne pense pas
que je suis \'à la mode\', mais mes vêtements sont confortables. Je
n\'aime pas porter les chaussures à la maison, mais je dois porter les
chaussettes car autrement j\'aurais les pieds froids.

Mon père est né à Bury, qui se trouve dans le nord de l\'Angleterre. Il
est avocat, et il est très drôle. Il adore peter et nous raconter les
blagues. Il est de taille moyenne, et il a les cheveux bruns et courts.
Il n\'est pas gros parce qu\'il aime courir et faire les sports avec mes
frères (comme le rugby).

Ma mère est née à Connahs Quay au Pays de Galles. Elle était avocate,
mais elle n\'est plus avocate car elle veut faire des choses pour
l\'environnement et elle voudrait être juge. Elle est très belle et
intelligente. Elle adore être avec notre chien, Ruby, et faire les
choses d\'avocat. Comme mon père elle est assez sportive, et elle court
beaucoup. Elle ne l\'aime pas quand moi et mes frères nous nous
disputons.

J\'ai deux frères qui s\'appellent Billy et Charlie. Billy est très
bruyant et embêtant, mais il chante bien. Charlie est sportif et gentil,
mais il n\'est pas trés doué avec les ordinateurs.

Finalement, j\'ai deux chats et un chien. Les chats s\'appellent Webster
et notre chien s\'appelle Ruby. Webster est très mignon et content, mais
Wanda est timide et grosse. J\'adore Webster. En plus, notre chien Ruby
adore faire des promenades et chercher les ballons et les balles.

J\'ai une tante qui habite en Allemagne, qui est la soeur de mon père,
et elle fait la musique pour avoir l\'argent. Elle s\'appelle Catherine.
Son mari aussi fait la musique. Pour les frères et les soeurs de ma
mère, j\'ai une tante qui est docteur, un oncle qui fait les films, et
un oncle qui est facteur.
