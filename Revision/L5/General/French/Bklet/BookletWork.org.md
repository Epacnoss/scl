---
author: Jack Maguire
title: French Booklet Work
---

# Venir De

## Present

1.  viens de voir
2.  vient de sortir
3.  venons de terminer
4.  viens de ranger
5.  venez de prendre
6.  vient de boire
7.  vient d\'expliquer
8.  viennent de se diriger
9.  vient de terminer
10. vient de recevoir.

## Imperfect

1.  venais de voir
2.  venait de sortir
3.  venions de terminer
4.  venais de ranger
5.  veniez de prendre
6.  venait de boire
7.  venait d\'expliquer
8.  venaient de se diriger
9.  venait de terminer

# Avant De

1.  avant de sortir. He finished his homework before going out.
2.  avant d\'utiliser la toilette She made her bed before going to the
    bathroom.
3.  avant de sortir au centre ville. He took a showever before going out
    in town.
4.  avant commencer travailler. They ?-ed before starting to work.
5.  avant de jouer sur mon ordinateur. I ate dinner before playing on
    the computer.
6.  avant de prendre le bus. She bought a ticket before taking the bus.
7.  avant d\'écrire mon essai. I did research before writing my essay.
8.  avant d\'aller en bas pour prendre mon petit-déjeuner. I got dressed
    before going down for breakfast.
9.  avant de prendre les examens. Before taking the exams, it is
    important to revise.
10. avant de manger He advised against bathing before eating.
11. avant de faire de l\'exercise Before doing exercose, I eat very
    little.
12. avant de decider quel subjects prendre Before deciding on the
    subjects I will study next year, I need more information on the
    programs.
