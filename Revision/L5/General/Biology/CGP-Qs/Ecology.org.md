---
author: Jack Maguire
title: Ecology Qs
---

**NB: If not marked, it is correct**

# Part 1

## Warm-Up Questions

1.  The Community.
2.  An ecosystem is the combination of a habitat (where organisms live),
    and a community (the organisms that live). **Plus the Biotic/Abiotic
    Factors**
3.  Lack of rain, hot sun, extreme snow/hail, small daylight hours.
4.  **?? - Not Covered/Relevant**
5.  The producer is the start of a food chain/web and gains sustenance
    from the sun.

## Exam Questions (4-6)

1.  1.  Tertiary Consumer
    2.  The algae is the producer, and all of the energy in the food
        chain comes from it, meaning that if it were to die out, all the
        other species in the food chain would die too.

2.  **Not Relevant**

3.  1.  Considering the fact that the lake trout are dominant over the
        cutthroat trout, as more and more lake trout are introduced, I
        would expect the Lake Trout population to soar over the
        Cutthroat population which would rapidly decline. However, there
        are only so many resources in the lakes, and so eventually the
        numbers should settle down.
    2.  ?? **Availability of Food/Competition**

# Part 2

## Warm-Up Questions

1.  A Transect is a line used to measure distribution along a line (say
    for shade vs non-shade).
2.  12 daisies
3.  Seasons, El-Nino, Climate Change
4.  They decompose dead things
5.  All of it. **Burning**

## Exam Questions (4-6)

1.  1.  Assimilation
    2.  Combustion **Respiration**
    3.  Detritivores decompose the dead matter which releases CO~2~
    4.  Photosynthesis.

2.  1.  Plant Evapotranspiration.
    2.  Firstly, on every day all water evaporates a bit, and when our
        water molecule evaporates, it gets carried on the wind, and
        eventually it forms (with lots of other water molecules) into a
        cloud. Eventually, the cloud passes over land, and when the
        pressure becomes lower, it eventually rains, and our water
        molecule can fall into a pond.

3.  1.  They would have randomly thrown quadrats, and taken averages to
        get average numbers of poppies for different distances.
    2.  As you get father and father from the wood, there are more and
        more poppies.
    3.  Distance from the stream, number of children nearby (who might
        pluck the daisies).
    4.  Species A - 46% **47%**; Species B - 48%;
    5.  To see if the daisies were more common in one area rather than
        another. **Wut? Blades of grass are hard to count**

```{=html}
<!-- -->
```
1.  Firstly, the smaller branches would be burnt which directly releases
    the Carbon Dioxide back into the atmosphere. Next, the green plants
    at the edge of the construction site would likely die from a lack of
    water/nutrients, and they would then be decomposed. Then, as they
    are decomposed, the Carbon gets released back into the atmosphere.
    Finally, the wood used for furniture would be used as furniture, but
    eventually discarded and eaten by mold, woodlice or bacteria, which
    would use respire and release Carbon Dioxide.

# Part 3

## Warm-Up Questions

1.  Biodiversity is how many different species live in one area.
    **Variety**
2.  CO~2~, CH~4~; Carbon Dioxide, Methane;
3.  For firewood, to clear space for a plantation.
4.  When the species are bred in captivity, their populations are
    increased, and this process becomes exponential, and we can
    gradually release animals back to the wild.
5.  Biodiversity can be increased in areas that farm single crops by
    ??????????????????????????????????????????????????????????????
    **hedgerows/field margins**

## Exam Questions

1.  1.  Carbon Dioxide from burning things, and Methane from the cows we
        eat farting.
    2.  When we create sewage, often it can overflow leading to the
        rivers getting polluted.
    3.  We are constantly increasing in both population, and demand.

2.  1.  Burning consumes oxygen, so less oxygen in the air; Extreme
        weather;
    2.  Carbon Reducing Agreements; Environmentally-friendly energy;

3.  **IRRELEVANT**

# Part 3

## Warm-Up Questions

1.  The different tropic levels describe how many organisms have been
    involved in the energy transfer before the current one. **Different
    stages of a food chain**
2.  Biomass is the amount of energy in an organism. **The mass of living
    material**
3.  If you can only fish a certain amount, then you cannot over-fish?
    **Fish too young not caught**
4.  **NR**
5.  **NR**

## Exam Questions

1.  1.  112.5g
    2.  11.25% efficiency. **18 / 112.5 - wrong two levels - 16%**

2.  **NR**

3.  **NR**
