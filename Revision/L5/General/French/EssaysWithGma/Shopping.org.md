---
title: Shopping
---

Chers blogueurs,

Aujourd\'hui j\'ai décidé de parler des magasins, les petits magasins et
les achats en ligne.

Récemment, je suis allé au centre commercial pour chercher un cadeau
pour mes petits frères. C\'était bien, et j\'ai trouvé un livre sur les
chats pour Charlie, et un petit livre sur les chiens pour Billy.

À mon avis, les achats en ligne sont formidables! C\'est très facile, et
plus vite que les magasins. En plus, il y a plus de choix pour beaucoup
de petites choses comme les ordinateurs.

Par contre, j\'aime les petits magasins parce qu\'ils ont plus de
personnalité que les grands magasins, et ils sont plus intéressants ---
les grands magasins sont tous les mêmes, mais les petits magasins sont
très différents.

Pendant les grandes vacances, je voudrais acheter beaucoup de jeux
vidéos pour mon excellent ordinateur, et de temps en temps j\'achèterais
des vêtements à Superdry. Mon magasin préféré pour les jeux vidéos est
Steam, parce que je peux utiliser Linux et on peut reprendre son argent
si on n\'aime pas le jeu.

À Bientot,

Jack
