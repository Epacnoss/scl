---
title: Lockdown
---

Il y a un an, le confinement a commencé, et tout le monde a dû rester à
la maison. Il y a des aspects positifs, des aspects négatifs et je vais
t\'en parler maintenant.

Au début, c\'était amusant car je pouvais me lever plus tard, et je
pouvais préparer mon propre déjeuner moi-même.

Mais vite j\'ai compris que ce n\'était pas bien - quelques profs
étaient très bien à l\'école, mais ils ne savaient pas très bien donner
les cours en ligne. En plus, quand les cours étaient ennuyeux, j\'ai
trouvé difficile de me concentrer.

Cependant, je pouvais voir ma famille aux repas et je pouvais préparer
leurs déjeuners car j\'aime cuisiner. En plus, j\'ai aimé courir aprés
l\'école et je n\'ai pas eu besoin d\'aller à l\'école.

Mais, à cause du confinement je ne pouvais pas voir mes amis, ou aller à
l\'étranger pour les vacances. Je peux discuter avec mes amis, mais je
ne peux pas aller chez eux, et ils ne peuvent pas venir chez moi. À mon
avis, les devoirs sont bien quand on n\'a pas trop de devoirs, car
c\'est très ennuyeux quand on n\'a rien à faire. Mes profs nous ont
donné vers dix minutes de devoirs par jour et je ne les fais pas
jusqu\'a la dernière minute.

Heureusement, maintenant il y a un vaccin (et plus de vingt millions de
personnes l\'ont recu (w/ cedilla)), et je peux aller à l\'école pas en
ligne et je peux voir mes amis. En plus, les régles vont être moins
strictes.
