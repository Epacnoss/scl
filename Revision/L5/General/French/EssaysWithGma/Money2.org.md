---
title: Money2
---

Cher bloggueurs,

Je vais vous parler de l\'argent de poche et mes opinions.

J\'ai un compte bancaire à la même banque que mes parents et ils peuvent
me donner l\'argent chaque mois. J\'ai l\'argent de poche, mais si j\'ai
des résultats formidables, mes parents me donnent plus argent.

Si les enfants ont de l\'argent ils vont comprendre combien les choses
coûtent, et c\'est plus facile pour les parents d\'être plus juste aux
enfants. En plus c\'est bien savoir comment garder l\'argent.
