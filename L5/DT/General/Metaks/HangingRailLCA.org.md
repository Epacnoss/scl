---
author: Jack Maguire
header-includes:
- "`\\usepackage[margin=1in]{geometry}`{=latex}"
- "`\\pagenumbering{gobble}`{=latex}"
title: Hanging Rail LCA
---

`\usepackage[margin=1in]{geometry}`{=latex}

`\pagenumbering{gobble}`{=latex}

# Extraction Of Resources

The Hanging Rail appears to be made from Wood, Metal (possibly Stainless
Steel), covered in Paint, with Screws/Rivets (likely also Stainless
Steel).

## Wood

To get usable Wood for this, we need to go to a forest and firstly chop
down trees. Historically, this would have been done by hand, but now we
use machinery, to chop down the tree, strip the branches and put it onto
a lorry. Obviously, this equipment has to be made, and they are composed
of metals, leather and plastics. For the metals, see below, for the
leather use the environmental impact of a cow, and the drying process,
and for plastics, they have to travel a long way, they have to be
harvested (often off-shore which takes even more metal and plastic), and
then refined. Depening on the refining process, it can have various
environmental impacts (although plenty of travel is needed for moving
components from place to place, like dye and off-products), but I don\'t
know plastic refining in Detail. All of this impact can be spread over
the hundreds (maybe thousands?) of trees one machine will cut over its
working life. We also have to have people to drive the machinery, but
for convienience, I will not take them into account. Then a lorry (more
metal, more leather, more plastic), will drive the trees to a
(hopefully) nearby factory. There, they will be weathered, dried,
removed of bark and cut. Dependent on the cutting method, there might be
wood waste. Then, this wood has to be transported to another factory,
which would cut it into the shape required. More metal, more plastic and
possibly a journey across the oceans again to get to the final factory.

## Stainless Steel

Stainless Steel is composed mainly of Iron and Carbon (in Coal form).
Both of those minerals can be found in Australia\'s mining industry,
although due to the size of Australia, we seriously need to take into
account the Trucks/Trains to carry the minerals to the ports. Also -
there is lots of machinery needed to find/make room to find the Iron Ore
in the Quarries. More metal, more plastic, plus lots and lots of fuel.
This then takes EVEN MORE Oil, which has to be refined via Fractional
Distillation. Then, once we have the Iron ore and Coal at a port, like
Perth, we need to ferry them across to a factory to smelt them to
produce Steel. These can be found in lots of places, but here we can
assume China. A Large ship designed for carrying minerals rather than
Containers like this one: <https://bit.ly/3sktvPh>. Then, to melt it
down, we need Huge amounts of energy, likely coming from a fossil fuel
plant, which pollutes the atmosphere. Also - we need crucibles and a
factory to support it, which needs more metal/concrete. Then, we can
take the finished shapes to our final factory, likely in plates.

## Paint

Paint typically consists of a Pigment, a Resin, a Solvent and Additives.
The Pigment should be relatively easy to find, but we might need to
transport it a long distance. The Resin, is likely plastic based, so we
have all that comes with Oil, and the Solvent might just be water. I
don\'t know about the Additives.

# Manufacturing

At our factory for producing these, we need machines to fold the steel,
machines to paint the steel, machines to rivet together the steel, and
machines to add the wood. For folding the steel, we can use presses at
certain points, and plasma cutters to get the shapes to fold. Plasma
cutters need lots of energy and lots of coolant, and presses need more
metal. Then, for all of the rest, we can use robot arms/human arms if
necessary. Robots can paint, with jets of paint, or you can electrodip
it to ensure adhesion and coverage. Then, the wood can be added, and
then all of it can be riveted together.

# Distribution

Likely, the Finished Products will have to travel long distances to get
to their customers, and in order to increase shipping efficiency, they
might be flatpacks, as there will be less air gaps. Then, we need brick
and mortar stores to sell them in (or large distribution centres for
e-retailers). Lots of trucks, ships and people.

# Use

Assuming the paint is non-volatile, the paint stays, and all of the
rivets/screws stay in, the Hanging Rail should have very little negative
Environmental Impact in use.

# EOL/Recycling

The wood can be reused, possibly as a rolling pin, or bat
(baseball/cricket), or further refined to paper, but if it is truly done
for, we can either burn it, or mince it to sell as cheaper paper/MDF.
The Paint cannot be reused. The Stainless Steel could be remelted down
to avoid the whole first part of refinement, but it might be too hot to
be economically viable. Possibly scrap.
