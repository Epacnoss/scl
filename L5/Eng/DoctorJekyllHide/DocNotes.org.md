---
author: Jack Maguire
title: Jekyll/Hide Doc Notes
---

# Qs

1.  Jan 1886
2.  35
3.  Treasure Island
4.  Builders of Deep Sea Lighthouses/Advocates for the Church.
5.  Allison Cunningham AKA \'Cummy\'
6.  Fundamentalist Christian, possibly a Calvanist
7.  Edinburgh
8.  Newtown, and Old town
9.  William Burke and William Hare
10. Bournemouth
11. Fanny van de Griff Osborne AKA Mother
12. He threw the manuscript onto the fire.
13. They said it was a more moral tale, so OK to read.
14. Richard Mansfield
15. 5 prostitutes were killed by Jack the Ripper, who was never found.
