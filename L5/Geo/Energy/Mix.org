#+TITLE: Energy Mix
#+AUTHOR: Jack Maguire

* How and why the UK’s energy mix has changed since 1970
In 1970, we used predominantly fossil fuels (38% coal, 48% oil, and 8% gas), with renewables being a small bit off to the side (3.5% nuclear power, <2% hydroelectric power, <2% renewables and <1% biomass). This was because we weren't aware of the negative effects of fossil fuels in the same way. In 2014, however, we had a lesser dependence on specific types of energy, even if it was still mainly fossil fuels. However, there are fewer fossil fuels being used. We had 30% coal, 30% gas, 19% nuclear, 9.5% wind, 6.8% bioenergy, 1.8% hydro, 1.2% solar and 1.8% other.

* The advantages and disadvantages of nuclear power
** Advantages
- Saves lives - has prevented about 1.8 million deaths including fukushima etc.
  + No coal mine accidents.
  + Lots of dangerous stuff in a deep hole, or lots and lots in the atmosphere.
  + Events singe in our minds, nuclear is like planes - only a few accidents, even if it works safely most of the time.
  + Saves more lives than it destroys due to not as many air pollution problems.
- Less CO_{2}
  + Many gigatons have not been released due to nuclear.
  + Coal is cheap, but is bad/unsustainable.
  + Compared to coal etc. it is clean. Maybe a middle man until we have better renewables.
- Most of our current nuclear plants are outdated.
  + Some new plants are incredibly efficient, or have dangerous materials for only a few hundred years, or be hard to turn into nuclear weaponry.
  + Lets at least do some research.
** Disadvantages
- Nuclear weapons proliferation:
  + Easier to start/hide nuke development.
  + Reactor tech has always been intimately connected with nuclear weaponry.
  + There are even treaties which try to help distribute reactor technology without nuclear weapon technology, but it isn't doing well.
  + 5 countries have developed nukes with help of reactors
  + The road to nuclear weaponry is paved with good intentions and safe, peaceful reactors.
- Plutonium and other radioactive materials.
  + Loses rads over 10s of thousands of years.
  + Can be reprocessed into nuclear weaponry, and nothing else because we don't have the right reactors (yet).
  + No proper powers trying to keep nuclear waste safe - right now it gets dumped in a lead-sealed hole.
- Nuclear Accidents
  + Only 7 or 8 incidents, but incredibly dangerous when it does happen.
  + Deaths in the 1000s, happening with different reactors in different countries in different times.
  + How far do we go - how many people need to die, or how much land needs to be declared unfit for human use until we stop.


* Types of renewables found in the UK
In the UK, we have wind, wave, hydro, biomass and solar, although biomass is recyclable rather than renewable. We have lots of offshore wind plants, as well as ones on land. Unfortunately, Solar isn't suited to the UK due to a lack of consistent sun, although that makes wave/wind plants all the better. We also don't have enough rivers shaped the right way for hydro plants, although we do have the Electric Mountain in Snowdonia in Wales.
 - Our offshore wind plants can generate as much power as the rest of the world combined, and there are currently 28 farms with 1500 turbines and 5GW. There are over 900 onshore wind farms with 5000 turbines and 8.5GW of power.
 - Biomass can be sourced from lots of living substances like food waste or animal manure, and it can even be burnt in conventional power stations. It can also be made into biodiesel or biogas.
 - Hydro-electric power is about 1.5% of the UK's electricity production, and there are lots of types ranging from dams to pumping water. Electric Mountain is the UK's largest Hydro-electric plant.
 - There are only a few wave and tidal power plants here in the UK, but they are expected to make a bigger contribution this and next year.
 - Even if we only gain 1.2% of our energy via solar power, we have had massive growth recently mainly due to government incentives. Installations range from solar farms to panels on roofs.

* Reasons why nuclear and renewables are likely to be more important in the future
As more and more of our fossil fuels are consumed, and we rely on them more and more, eventually they will run out. That, and also the fact that we are facing a climate disaster, of which one of the major causes is our addiction to fossil fueled power plants. This means, that over the next few years rather than the next few hundred years, we will have to switch completely to renewable or low-carbon power sources. Renewables are important because they go for the forseable future, and they don't pollute our environment nearly to the same degree (even if they still do pollute a bit, as lots require a lot of concrete). However, our renewable technology is not quite ready for a transition yet, so lots are proposing we use nuclear energy as a middle man to stop polluting the environment, but still have clean, reliable energy until we have all of our other renewables completely worked out. However, nuclear fission must be used as a middle-man, rather than a long-term solution, because right now we do not have a long-term solution to the nuclear waste.
