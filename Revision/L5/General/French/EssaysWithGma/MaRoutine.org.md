---
title: Ma Routine
---

Aujourd\'hui je voudrais vous parler de ma routine.

Pendant la semaine, je me réveille vers huit heures et demie, car il ne
faut pas aller à l\'école à cause de la Coronavirus. Après ca (w/
cedilla), normalement, je m\'habille et je prends mon petit déjeuner
(des céreales avec du lait froid), et finalement, je retourne à ma
chambre, et je me brosse les dents.

Ensuite, j\'ai mes cours qui commencent à neuf heures et quart (mais je
dois être à mon ordinateur à neuf heures moins cinq. Comme ca (w/
cedilla), mon prof sait que je suis prêt pour la journée).

J\'ai trois cours, une petite récré de quinze minutes, et puis deux
autres cours. Je prends le déjeuner vers une heure moins le quart, et
mon plat préféré est des tartines au jambon et fromage. Finalement,
l\'après midi j\'ai trois cours. Et puis je me repose. Pour me détendre,
je joue aux jeux vidéos, ou je regarde un film.

Pour les taches ménagêres, je ne dois pas faire beaucoup. Je dois mettre
la table avant le dîner (avec le couteau, la cuiller, la fourchette, un
verre et une assiette), et parfois je prépare le plat car j\'aime
cuisiner. En plus, je dois faire mon lit.

Pour trois weekends il y a quatre mois, ma femme de ménage ne pouvait
pas venir chez moi, et donc toute ma famille a dû nettoyer la maison, et
il fallait quatre heures! Heureusement, elle pouvait retourner chez moi
en septembre. Cependant c\'est très utile de savoir comment faire le
ménage.
