---
author: Jack Maguire
title: Xmas Essay 2
---

La fête de Noël est importante pour moi parce que je peux voir les
membres de ma famille et recevoir des cadeaux.

Un Noël typique pour moi est au Pays de Galles avec beaucoup de
personnes.

Je me souviens bien d\'un Noël il y a trois ans, au Pays De Galles avec
trois familles dans une petite maison.

Je veux passer le Noël l\'année prochaine au Pays de Galles, avec
beaucoup de personnes.
