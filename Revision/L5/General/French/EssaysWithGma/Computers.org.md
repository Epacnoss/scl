---
title: Computers
---

J\'aime rester un contact avec mes amis pendant ce temps avec mon
ordinateur. Ma façon préférée est Discord, car je peux avoir un endroit
pour beaucoup de mes amis, avec beaucoup d\'autres petits endroits pour
les différents sujets. C\'est une évidence que je voudrais regarder mes
amis, mais Discord et Zoom sont bien.

J\'ai beaucoup utilisé mon ordinateur et l\'internet pour l\'école - je
peux trouver chaque mot pour les langues, ou regarder les expériences
pour les sciences. Je le trouve trés intéressant.

Il est évident qu\'un mois sans l\'internet ne serait pas amusant pour
moi, alors je ferait beaucoup d\'exercises, par example du cyclisme,
faire promener le chien au jouer du golf avec mes parents, et jouer aux
jeux vidéos. Aussi, je peux jouerais plus du basson. En plus, je
jouerais avec mes petits chats et mon chien noir.

L\'internet est super pour la coronavirus car je peux discuter avec tous
mes amis, et je peux chercher les nouvelles distractions. Cependant, il
y a beaucoup de mauvaises personnes sur l\'internet.
