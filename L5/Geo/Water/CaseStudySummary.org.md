---
title: Case Study Summary
---

# Para 1

-   The availabilty of water aid nations in everything, from production
    to population.
-   80% of the world\'s population live in areas where a fresh supply of
    water is not secure, and with China\'s population of 1.3bn this is a
    worry for the Chinese government.
-   Lots of Chinese rivers like the Yellow River have been polluted in
    their conquest for more water. Also - there is a disrepancy in where
    the water is in China, with the Northern provinces having less
    water, which has led to projects like the South-North transfer.

# Para 2

-   In China, 65% of water use is from agriculture, 23% is industry and
    a measly 12% is from domestic use.
-   The main reason so much water is used in agriculture is because of
    the inefficiencies in watering the crops. This is a worry, because
    as China\'s population grows, her water capacity will not be able to
    keep up, so this needs to be rectified.
-   This incredible demand could lead to legislature to reduce domestic
    water use, like rationing or hose-pipe bans. The Chinese Government
    has also already banned certain TNCs such as walmart for their
    excessive water usage.

# Para 3

-   Lots of Chinese stores have already pledged to cut their water use
    in half, at over 155 outlets in the past 2 years. This is a good
    step, as Industry is the second highest water consumer in China.
-   Another issue for China\'s water supply is their growing middle
    class, whom are using more and more water than the lower classes.
    They also use appliances like Washing Machines which consume far
    more water, as well as their diets, which will involve more
    expensive foods with more invisible water (like Red Meat).
-   Finally, with the middle class, is that they will consume products
    themselves which will increase Industrial Demand, which in turn
    needs more water.

# Para 4

-   One Technical Solution is a transfer project called the South-North
    Transfer project, which by 2050 plans to transfer water through 3
    routes - Eastern, Western, and Central.
-   This project will help to increase Water Security in the outer
    provinces, which are mainly used for agriculture.
-   This is a long term solution, that could bring prosperity to outer
    provinces.

# Para 5

-   Transboundary rivers are a large problem, and conflicts are seen
    everywhere in the Middle East, such as in Egypt, Syria, Turkey,
    Jordan and Israel. The Himalayas are known as the \'water tower of
    asia\', as the mountain range provides lots of water every year from
    meltwater, which flows into the surrounding rivers such as the
    Brahmaptura which is shared by many countries (always a bad idea),
    such as China, India and Bangladesh.
-   In order to increase their water security, China is currently
    building a hydroelectic dam in Tibet, which will provide water and
    power, but India and Bangladesh who are downstream have spoken out
    about the dam, as they say the flow will be affected. China says
    this won\'t happen
-   A similar situation exists with the Mekong River.

# Para 6

-   Another major factor that affects China\'s water security is the
    pollution of the country\'s water bodies.
-   Some Chinese rivers have been declared as unsafe for human
    consumption, and it can only be used in industry now.
-   More than 4 trillion litres (from 4 billion tonnes) have been duped
    into the Yellow River in 2015 alone, and this is 88 billion more
    litres more than the previous year.
