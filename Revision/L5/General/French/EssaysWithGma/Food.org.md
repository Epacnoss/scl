---
title: Food
---

Je pense que la nourriture est très importante. Je vais vous en parler.

D\'abord, je vais vous parler de ce que je mange normalement. Au
petit-déjeuner, aujourd\'hui j\'ai mangé des céréales avec du lait
froid, mais j\'adorerais si j\'avais mangé des crêpes avec du sucre ou
du jus de citron.

Au déjeuner, il y a beaucoup de choses que je peux manger. Je peux
manger une omelette (au jambon et fromage), ou des pâtes, ou des oeufs
brouillés.

Le soir, je prends mon dîner, et normalement c\'est des frites avec du
poulet ou du poisson. En plus, nous prenons des légumes (par exemple les
petits pois, ou le broccoli). Et comme dessert, nous prenons un fruit.

Ma nourriture préférée est la cuisine espagnole. Par exemple, j\'aime
les tapas, qui se composent de beaucoup de petits plats (du saucisson,
ou du jambon, ou du fromage ou des pommes de terre).

Quand je suis chez moi, j\'aime faire la cuisine, mais pour la plupart
du temps, c\'est ma mère qui la fait parce qu\'elle est la plus vite
pour cuisiner.

Par exemple, aujourd\'hui ma mère va préparer le dîner, et je ne sais
pas ce que nous mangerons, mais j\'espère que c\'est quelque chose que
j\'aime bien.
