---
author: Jack Maguire
title: Villi Question
---

Firstly, the blood capillaries are useful because they have the thinnest
walls, so diffusion is easiest through them. Then, due to the shape
(long/thin), it has the highest surface area with the lowest volume
which leads to more diffusion. Finally, there are few cells betwee the
food and the capillaries making diffusion even easier.
