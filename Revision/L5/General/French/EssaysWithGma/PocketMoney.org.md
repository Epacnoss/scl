---
author: Jack Maguire
title: Pocket Money
---

Aujourd\'hui j\'ai décidé d\'écrire un blog au sujet de l\'argent de
poche.

Moi, j\'aide mes parents dans la cuisine, je donne à manger à mes petits
chats, et je range ma chambre. Quand je l\'ai fait, mes parents me
donnent £12.04 par mois.

À mon avis, il est évident que les enfants devraient avoir leur propre
argent. Je le pense car c\'est important de savoir comment utiliser et
garder l\'argent.

Récemment, avec mon argent j\'ai acheté un grand ordinateur pour jouer
aux jeux vidéos. C\'est formidable et ça me plaît beaucoup.

Je ne sais pas si les enfants devraient travailler. C\'est pour chaque
parents de décider combien d\'argent il devrait doner à pour leurs
enfants, et si leurs enfants devraient travailler. Je pense que c\'est
très imporant d\'avoir de l\'argent de poche, mais certains parents
n\'ont pas assez d\'argent pour l\'argent de poche. Pour eux, leurs
enfants pourrait travailler. Je pense que les parents devraient décider
combien d\'argent de poche ils peuvent donner à leurs enfants.
