---
title: Adaptations In Trop Rain
---

# Task 1: Main Characteristics of Each Layer

## Emergent:

-   Direct heat from Sun.
-   F/F adapted to bright sun/strong winds/rain.
-   Tree branches thin (can\'t support animals)
-   Animals small, usually fly/glide.
-   A/I include :: birds/small monkeys.

## Canopy

-   60-90% of all F/F.
-   Warmest during the day.
-   Animals adapt by making loud calls/sounds to communicate.
-   A/I include :: Toucans, Lizards, Monkeys, Rodents & Tree Frogs.

## Undercanopy

-   V. humid + damp.
-   Many plants + mosses/lichens/fungi grow in this layer.
-   Insects found here due to very little seasonal change.
-   A/I include :: Beetles, Butterflies, Jaguars & Snakes

## Forest Floor

-   Darkest/Most humid.
-   A/I include :: Jaguars, Anteaters & Bearded Pigs.

# Task 2: Why so much biodiv?

-   Lots of nutrients + layers which F/F are adapted to.

# Task 3: S/B + Nut Transfers

The majority of all the nutrients are stored in the biomass, rather than
the soil, meaning that when farmers take down all the trees and burn
them, the plants don\'t grow.

# Task 4: Animal Life in the Rainforest

In a TRF, unlike lots of other climates, the majority of animal life is
off the ground in trees, due to a lack of light and nutrients on the
Forest Floor.

# Task 5: Adaptations

## Fast Growing Trees

These trees need to grow fast to ensure that when the nearby trees make
space, the new trees can sprout yup before the other trees to get the
sunlight.

## Epiphytes

Epiphytes are organisms that grow on the surfaces of plants and trees,
and get their moisture from the air around the plant it is attached to.
They have adapted to do this, as it is hard to get the sunlight at the
top without being on a tree.

## Thin, smooth bark

Lots of trees have thin smooth bark, in order to make it harder for
insects and plants like epiopytes to gain a foothold, and when they do
get a foothold, it is easier to shed to get rid of parasites.

## Lianas

Lianas are wooden vines that have roots in the ground (unlike
Epiphytes), but use trees as vertical support (like Epiphytes) to grow
leaves nearer the sun.

## Flexible Leaf Base

The leaves on trees often have flexible bases to allow them to still
catch the sun, but to also allow the rain to fall onto the roots.
