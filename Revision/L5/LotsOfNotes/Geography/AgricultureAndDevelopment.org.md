---
author: originally by TCH, transcribed by Jack Maguire
header-includes:
- "`\\usepackage[margin=0.5in]{geometry}`{=latex}"
title: Agriculture And Development
---

`\usepackage[margin=0.5in]{geometry}`{=latex}

# Agriculture

## Causes of Food Shortages

-   Human Factors:
    -   Population increase which leads to greater consumption of food,
        increasing prices due to hight demand, and low supply.
    -   Increased pop. has also led to greater areas of land required to
        grow crops, destroying settlements and leading to overcrowding
        in certain areas.
    -   Destruction of forests has led to increased rate of c.c. asw
        well as desertification and prolonged droughts.
    -   Increased pop. has also led to increased pollution, as more
        people are using fuel to power cars/homes/gas for cooking.
    -   The resultant effect is an increase in water/air pollution,
        affecting the climate as well as food production.
-   Environmental Factors:
    -   Increased use of fossil fuels has increased levels of pollution,
        desertification, and eutrophication which has led to land
        degradation through loss of space and fertility of lands.
    -   Natural Disaster rates have also increased due to c.c., as well
        as their severity and length. Drought is the leading cause of
        food shortages in the world, and consecutive years of prolonged
        droughts have led to massive crop failures in Africa and Central
        America.
-   Economic Factors:
    -   Increased levbels of poverty in developed nations have meant
        that farmers are unable to invest in seeds, fertilisers or new
        technologies. This results in an increase of cheap, inefficient
        farming methods driving up food prices, meaning citizens cannot
        afford food, even if it is available.
    -   Global financial crises such as the Covid-19 pandemic have meant
        that foreign governments are unable to invest in developing
        nations and there is less availability for financial aid from
        groups such as the UN.

## Solutions for Food Shortages

-   Carbon emissions have to be reduced as well as pollution and
    deforestation in order to reduce the effects of c.c.
-   There is also a need to invest in clean, sustainable energy sources
    and aid needs to be given to developing nations to help implement
    this.

## Case Studies

### Intensive Farming in Swaziland

-   Swaziland has 4 physical input land zones:

    High Veld
    :   eroded and thin soils, steep slopes.

    Middle Veld
    :   rich soils, gentle slopes.

    Low Veld
    :   some rich soilds, flat land.

    Lubombo Uplands
    :   good, thin red clay soils, steep slopes.
-   Labour is typically from family members, with hand tools and little
    fertiliser used. Simple irrigation.
-   Cattle graze on Swazi National Land after April.
-   Pastures are burnt to get rid of coarse, dry grass.
-   Most farms are subsistence, with mostly maize grown, and few cattle.
-   Farmers are encouraged to join farmer\'s associations.
-   Harvested Maize is milled at the farmer\'s home to produce flower,
    which is then baked into bread to be eaten by the family. There is
    production of millet and vegetables, and the cattle provide meat and
    milk.
-   Erratic rainfall causes droughts, with water for irrigation not
    accessible to many. Farmers cannot afford to imprve, and roads are
    not good in remote areas, with soil erosion and animal disease
    rampant.
-   Investment is needed to improve irrigation, with use of crops that
    require less water. Farming inputs such as fertiliser or seeds
    should be subsidised.

### Famine in Somalia

-   Somalia has been affected by food shorages due to it\'s location in
    the Sahel. Drought in somalia has caused people to farm intensively,
    which exposes soils to high winds and rainfalls.
-   There is also some civil conflict in Somalia due to a disregad for
    authority, and a lack of infrastrucure from colonial rule. Citizens
    have taken up arms against the goverment, and have intercepeted
    international aid.
-   Local communities are unable to fund rudimentary healthcare systems,
    allowing diseases such as HIV or malaria to hinder their workforce.
-   The working population is also reduced, as lots of Somalian people
    are malnourished, lowering harvest and planting ability.
-   Famine has casued malnourishment and a weak workforce which is prone
    to disease.
-   Farmers gave over-farmed crops, which has ed to soil loss thorugh
    wind erosion as well as desertification.
-   Children are tempted into becoming child soldiers, resulting in
    piracy, interception of international aid and a reduced workforce.
-   This has led to the government abandoning investment in sustainable
    infrastrucure, causing further malnourishment and political
    instability.
-   Food shorages have been combated by crearive, simple irrigation
    systems by containers at the base of rocky outcrops and hillsides to
    catch ater.
-   International Aid has been given to Somalia, and equipment needed to
    improve agriculture has been provided.

### Effects of food shortages:

-   Short term effects:
    -   Increased deaths due to hunger and malnutrition.
    -   Reduced academic concentration and ability.
    -   Reduced focus on helping children to develop properly due to
        increased tiredness of mothers.
    -   Reduced life expectancy and increased infant mortality rate.
    -   Increase in civil wars and refugees.
-   Long term effects:
    -   Increase in food prices and demand.
    -   Increased cost of food production.
    -   Increase in cost of inputs such as fertilisers, transportation,
        agriculture and machinery.
    -   Increased political instability and extremism, leading to
        increased social unrest and equality.
    -   Decrease in food stockpiles in case of emergency or natural
        disaster, increasing the damage of natural disasters.

## Soil

-   Fertility is important in determininh the type of crops to grow and
    which processes to use (eg. the use and quantity of fertilisers).
-   Good Drainage systems reduce waterlogging, but require large amounts
    of capital to put in place and maintain.
-   Adverse weather conditions may mean thatharvest is delayed or
    destroyed resulting in a loss of capital (if the farm is
    commercial).
-   Wheat prices fluctuate according to supply and demand and so farmers
    must remain competitive, and continually invest in research of more
    efficient farming techniques and technologies.

## Farming General

-   Physical inputs are naturally occuring things such as water or raw
    materials. There are many humans and cultural inputs, such as money,
    labour or skills. Inputs are **any** external source that can help
    in the agricultural cycle.
    -   Processes are the actions within a farm that allow the inputs to
        turn into outputs. Processes include milking, harvesting, and
        shearing. Processes greatly vary between farm types, and there
        is lots to do with either crops (eg. Ploughing, Planting), or
        animals (eg. Grazing, Lambing).
-   Outputs can be either negative or positive. Negative outputs include
    waste products, soil erosion, the leeching of chemicals into the
    soil. Positive outputs are the finished products, such as meat, milk
    and eggs, and the money gained from the sale of these products.
-   Feedback is the profit gained to be used as an input the following
    year, and the knowledge of manufacturing. negative or positive.
    Negative outputs include waste products, soil erosion, the leeching
    of chemicals into the soil. Positive outputs are the finished
    products, such as meat, milk and eggs, and the money gained from the
    sale of these products.
-   Feedback is the profit gained to be used as an input the following
    year, and the knowledge of manufacturing.

### Inputs

Labour
:   In LEDCs, farmers use abundant cheap labour instead if machinery
    whereas in MEDCs, where labour is more expensive (eg. UK), machinery
    is used. This results in a more efficient farming system, that is
    more intensive.

Capital
:   Capital can be used to increase the inputs into a farm (eg.
    machineryl, renewing buildings). This will increase yields, and can
    generate further profits for future investments.

Technology
:   Machines and irrigation can help to increase yields, as well as
    greenhouses with controlled temperatures, and genetic engineering.
    However, these new technologies wil require large amounts of
    capital.

-   Relief:
    -   Lowlands, such as flood plains, are fertile and good for growing
        crops.
    -   Steep slopes also prevent useage of machinery such as tractors,
        and have thinner soils that are more prone to soil erosion.

### Types of Farming

-   Arable/Pastoral Farming:

    Arable Farming
    :   Farms that only grow crops.

    Pastoral Farming
    :   Farms that only rear animals.

    Mixed Farming
    :   Farms that both grow crops and rear animals
-   Commercial/Subsistence Farming:

    Commercial Farming
    :   Farms that only produce food for sale.

    Subsistence farming
    :   Farms that only produce enough food for one farmer and his
        family (sometimes with a little left to sell).
-   Intensive/Extensive Farming:

    Intensive Farming
    :   Farms that use large amounts of machinery, money and technology
        or workers.

    Extensive Farming
    :   Extenive Farms have smaller inpus, but way more land.

# Development & Globalisation

## Globalisation

-   Globalisation is the process by which national and regional
    economies, societies and cultures have been integrated through the
    global network of trade, communications, immigration and transport.
-   Globalisation is caused by a number of factors:
    -   Improved transport that is more affordable, efficient and
        reliable.
    -   The adoption of transport containers, allowing for cheaper
        transport of greater goods.
    -   Improved technology, which makes the communication and sharing
        of information easier. It also reduces technological labour
        costs, as labour is often cheaper in Asia than Europe.
    -   Increased Mobility of labour has meant that people are willing
        to move between different countries in search of work, and has
        increased global trade remittances (which plays a large role in
        the development of a country).
    -   Growth of TNCs with a prescense in many different countries has
        allowed for more jobs, as well as an increase in the spread of
        global cultures and ideas.
    -   Growth of social medias such as Instagram, Twitter and Reddit
        have also allowed for increased spread of cultures and ideas, as
        well as increasing tourism, and highlighting key political ideas
        or movements across the world.
    -   Improved mobility of capital through the decrease in trading
        barriers has meant that capital can easily flow between
        countries and economies.
    -   This has also increased the connections between global financial
        markets.

## Development Indicators

A Country\'s rate of development and development level can be measured
using indicators of development. Examples of these are:

GNP Per Capita

:   The total value of goods and services provided by a country (within
    that country) in a given year, divided by the total number of
    citizens.

    -   But, the GDP per capita does not take into account the informal
        sector, sustainability of economic growth, or environmental
        sustainability.

Literacy Rates

:   The percentage of adults who can read and write.

    -   Literacy Rates do not take into account non-academic skills that
        are equally valuable (eg. farming techniques)

Life expectancy

:   The mean age lived by people in a countru at birth.

    -   Dramatic advancements in medicine and technology can mean that
        life expectancy for a certain age group is inaccurate.

HDI

:   A measure of social and economic development, based on: average
    years of education.

    -   HDI reflects long, not short-term changes, and may average out 3
        high indicators and 1 low one, providing a less accurate view of
        development.

## Reasons for a lack of development

There are many factors which may affect why a country is unable to
develop, or why there are large inequalities between and within these
countries.

-   Firstly, a good political regime is needed for a country to develop.
    If the political regime is corrupt, such as in Lebanon,
    international aid may not be effectively distributed, legal or
    regulatory may be exploited and many funds can be embezzled or
    offshored instead of being used to improve infrastructure.
-   If a country is susceptible to natural disasters (eg. Japan), the
    government will have to continuously spend money on rebuilding
    infrastructure, or as wrork on natural disaster prevention such as
    through making more shake-proof buildings, or inventing an early
    warning signal and efffective, efficient communication.
-   A landlocked country will also suffer economically, as there will be
    reduced access to sea for the exporting of goods, lowering GNI. It
    is also likely surrounding countries are at war with each other (eg.
    Afghanistan).
-   Climate-related diseases such as malaria may cause large economic
    impact in one area of the world, or even within a country itself.
    This is a greater issue in LEDCs, which typically have warmer
    climates, and a greater reliance on the primary and secondary sector
    (manual labour). This means that they will be more affected by the
    epidemic and the workforce may be hindered and significantly reduced
    (including Cattle)

## The 4 Sectors

### The actual sectors

-   Primary Sector:
    -   The extraction and collection of natural resources such as
        through quarrying, or activities such as farming and fishing.
    -   The primary sector tends to take up a large proportion of the
        economy in developing countries.
    -   It offers low-skilled, low-paying manual labour jobs.
-   Secondary Sector:
    -   Raw materials extracted by the primary sector are processed into
        manufactured goods.
    -   Exmaples of this include manufacturing, food processing and
        energy production.
    -   The secondary sector takes up a greater proportion of a
        developing economy (eg. India) than a MEDC.
-   Tertiary Sector:
    -   This is the service sector, and involves the selling of services
        and skills. It may also include selling goods from the primary
        and secondary industries.
    -   Examples include transportation, tourism, or retail.
    -   This is a more prominent sector in MEDCs.
-   Quaternary Sector:
    -   Information services such as IT, consulting and R&D are
        provided.
    -   This is also a service and is only present in MEDCs, in which
        employment is steadily growing.

### The Fisher Model

-   Countries with higher levels of economic development (eg. UK)
    typically have a more prominent tertiary/quaternary sector, while
    less developed countries (eg. Nepal) have significantly less
    prominent levels of tertiary and almost no quaternary employment.
-   Developing countries typically have an employment structure
    dominated by primary and secondary industries, which decreases as
    the country becomes more developed, due to higher-paying jobs and
    therefore increases GDP.
