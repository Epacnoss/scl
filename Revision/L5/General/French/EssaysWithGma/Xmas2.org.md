---
title: Xmas2
---

Cette année, nous avons célébré Noël d\'une façon differente. Mais
normalement, nous allons au Pays de Galles avec nos petits chats mignons
qui s\'appellent Webster et Wanda, et notre beau petit chien qui
s\'appelle Ruby. Pour Noël nous voyageons au Pays de Galles en voiture,
mais c\'est très long et ennuyeux. Quand nous allons au Pays de Galles,
nous achetons un sapin de Noël, et les chats l\'adorent parce qu\'ils
peuvent y grimper. Pour la veille de Noël, normalement c\'est ennuyeux,
et nous ne faisons rien. Au contraire, la journée de Noël est trés
intéressante parce que nous invitons des membres de notre famille, comme
nos grandparents et les cousins. La dinde est toujours délicieuse, et
nous avons trop mangé. Il y a beacoup de cadeâux.

Malheureusement cette année Noël était très différent. À cause de la
Cornavirus, nous ne pouvions pas inviter de membres de notre famille, et
nous devions rester à Londres, où il faisait froid. En plus, il n\'y
avait rien à faire, et alors j\'ai fait des jeux vidéos dans ma chambre,
et j\'ai discuté avec mes amis en ligne. C\'était très ennuyeux. Au
contraire, la dinde était formidable, parce que mon père n\'avait rien à
faire, sauf cuisiner la dinde.

En 2021, je passerai Noël avec beaucoup de membres de ma famille et mes
amis, au Pays de Galles. Parce que notre maison est grande, nous pouvons
inviter 18 personnes, et ce sera très amusant. Nous mangerons une dinde
délicieuse et nous donnerons et recevrons beaucoup de cadeaux. En plus,
parce que la maison se trouve près de la mer, nous pourrons faire des
sports nautiques
