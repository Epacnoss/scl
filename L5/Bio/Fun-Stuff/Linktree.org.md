---
author: Lots of People
title: Linktree
---

# General Knowledge

## L

A place where you are taken to be tried of criminal charges
:   Court.

The inter [\_]{.ul}
:   net.

Sphere-shaped object used in sports
:   Ball.

An object with a handle and a set of strings
:   Racket Link - Tennis

## I

What does the L in TLS stand for
:   Layer. (Relating to Transport Layer Security)

What does the H in HTTPS stand for
:   Hyper. (Relating to HyperText transfer protocol)

What does the R in RFC stand for
:   Request. (As in Request for Comment)

What does the first S of SSH stand for
:   Secure (As in Secure Socket Shell. Link - The Internet

# Bio

## N

Mechanical digestion helps to increase the surface [\_\_]{.ul} to Volume Ratio
:   Area.

Saliva is a mix of: amylase, water and [\_\_\_]{.ul}
:   Mucus .

What is the most commonly used acid in the body
:   Hydrochloric Acid.

Pepsin breaks down proteins into what
:   Polypeptide Chains. Link - Food

## K

A Pyramid of Biomass looks like an Egyptian pyramid rather than a [\_\_\_]{.ul} pyramid
:   Aztec.

When fertiliser enters a river, [\_\_\_\_\_\_\_\_\_\_\_\_]{.ul} can happen
:   Eutrophication.

Sedimentation is going from Plants to [\_\_\_\_]{.ul} [\_\_\_]{.ul}
:   Fossil Fuels.

Organisms involved in Decay are called:
:   Decomposers. Link - Carbon Cycle/Evironment

## E (To APH - these questions need to be delivered in order preferably)

What colour Benedicts Solution go in the presence of Sugar
:   Red/Orange

What colour does Iodine go in the presence of Proteins
:   Yellow/Brown

What colour does Benedicts Soloution go in the prescense of Starch
:   Blue

What colour does iodine go in the presence of Starch
:   Purple. Link - The Rainbow!

# Christmas

## E

If its still healthy but not a fruit, its a [\_\_\_\_\_\_\_]{.ul}
:   Vegetable.

The 4th colour of the rainbow
:   Green.

The name of a 3D Circle
:   Sphere

The Country where the EU\'s HQ is
:   Brussels Link - Brussel Sprouts
