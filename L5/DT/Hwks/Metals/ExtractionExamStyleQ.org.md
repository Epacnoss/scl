---
author: Jack Maguire
title: Extraction Exam Style Question
---

The extraction of Raw Materials for the Production of Metals from the
Environment has a large impact on the environment, and it does this in a
variety of ways. Firstly, in order to extract raw materials, we need to
find said raw materials, and often this is done by digging large
quarries. This often requires large machines to excavate which require
fuel which has more impact on the environment. Also, as with any large
or small excavation project, you have to consider what used to be where
the hole will be, and where all of the useless dirt and stone will go.
This can cause visual pollution, but that is less relevant to the
environment. More directly related to the environment is what is/lives
where the hole is. It could have lots of native wildlife, or historical
artifacts. Then, the dirt has to be removed from the ores, and the ores
have to be smelted to create useful metals, which often takes lots of
power.
