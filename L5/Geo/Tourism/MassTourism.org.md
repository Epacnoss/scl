---
author: Jack Maguirea
title: Mass Tourism in Spain
---

# Features that attract tourists to Spain/Benidorm

## Physical:

-   Sunny Climate
-   Picturesque landscape
-   Local traditions
-   The Spanish voted the country with the best beaches.
-   Benidorm has 3 theme parks.

## Human:

-   Attractive Exchange Rate
-   Active promotion from the government.
-   Cheap, fast flights.
-   Package holidays - \'Sun, sea, sand and sangria\' at an affordable
    price.
-   Benidorm, has lots of bars and clubs with free entertainment.

# Positives and Negatives of Tourism to Spain

## Positives:

  Economic                                                                Social   Environmental
  ----------------------------------------------------------------------- -------- ---------------------------------------------------------------------------------------------------
  Obviously, lots of tourism to Spain, so money for the Spanish people.   ?        More people coming, means more income which means more money that can be spent on the environment

## Negatives:

  Economic                                                  Social                                    Environmental
  --------------------------------------------------------- ----------------------------------------- -------------------------------------------------------------------------------------------------------------------------
  Can damage local business in the storm to help tourism.   Local traditions can also be destroyed.   More people flying in means more environmental damage, plus all of the single-use plastics in the hospitality industry.
