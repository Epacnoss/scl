---
title: Poitiers
---

Quand j\'étais à Poitiers, je me levais tous les matins à sept heures.
Je me lavais, je m\'habillais, et je prenait mon petit déjeuner. Je
buvais du café et je mangeais du pain sec. Puis, je commençais mon
travail. Je travaillais jusqu\'à midi, et puis je voulais me reposer,
alors je sortais et je rencontrais mes amis au café.

Après le déjeuner, nous allions à un petit jardin près du collège ou,
nous nous premenions sur la place. À deux heures, je retournais à ma
chambre et je continuais mon travail.

Une heure avant le dîner, j\'allias au parc. C\'était le printemps, et
les oiseaux chantaient dans les arbres. Et il y avait un autre bruit que
j\'entendais quand je me promenais au parc. Un jour, j\'ai découvert ce
que c\'était. C\'était le bruit de grenouilles dans le petit lac.
