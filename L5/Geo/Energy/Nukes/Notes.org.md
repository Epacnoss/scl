---
title: Nuclear Notes
---

# Intro

-   Started in 1940s, post cold war.
-   Everyone went nuts thinking everything would be atomic.
-   Turns out it would be hard/expensive.
-   Plus v. risky, so people wouldn\'t invest.
-   Some didn\'t give up though.
-   Nuclear\'s finest hour was when oil prices skyrocketed, and more
    than 50% of the reactors were built between 1970 and 1985.
-   We all went with the Light Water Reactor, which is cheap, not too
    bad and already existing.
-   Really heavy elements get bombarded with neutrons, the atom explodes
    and causes a controlled chain reaction. Then, it is heated and
    turbined.
-   Not the safest, best or anything good tho.
-   Lots of disasters have happened with them.
-   Whilst in the 1980s, we had tons of plants being built... No none.
-   About 439 reactors in 41 countries.
-   160 new reactors planned rn.
-   More than 80% are light water.
-   Many countries are faced with a choice - renewables/old stuff OR
    upgrade plants.

# Why Awesome

-   Saves lives - has prevented about 1.8mn deaths including fukushima
    etc.
    -   Waste is normally stored, but ffs are pumped into the atmo.
    -   No coal mine accidents.
    -   Lots of dangerous stuff in a deep hole, or lots and lots in the
        atmo.
    -   Events singe in our minds, nuclear is like planes.
    -   Saves more lives than it destroys.
-   Less CO~2~
    -   Tons of gigatons have not been released due to nuclear.
    -   Coal is cheap, but is bad/unsustainable.
    -   Compared to coal etc. it is clean. Maybe a middle man.
-   Most of our current nukes are outdated.
    -   Some new are incredibly efficient, or have dangerous materials
        for only a few hundred years, or be hard to turn into nukes.
    -   Lets at least do some research.
-   Risks involved, but fortune favours the bold.

# Why Horrible

-   Nuclear weapons proliferation:
    -   Easier to make/hide nuke development.
    -   Reactor tech has always been intimately connected with nukes.
    -   There are even treaties.
    -   5 countries have developed nukes with help of reactors
    -   The road to death is paved with good intentions.
-   Plutonium etc.
    -   Loses rads over 10s of thousands of years.
    -   Can be reprocessed into nukes, and nothing else because we
        don\'t have the right reactors.
    -   No proper powers trying to keep nuke waste safe.
-   Accidents
    -   Only 7 or 8 incidents, but incredibly dangerous when it does
        happen.
    -   Deaths in the 1000s, happening with different reactors in
        differet countries in different times.
    -   How far do we go - where is the line.
