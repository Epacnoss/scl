---
title: Habiter en Ville ou à la Campagne
---

Il y a deux facons (w/ cedilla) de vivre en Angleterre. On peut habiter
en ville ou à la Campagne.

Maintenant, j\'habite en ville, et je l\'aime parce qu\'il y a beaucoup
de choses à faire le weekend. Je peux aller au cinéma, ou au musée, ou
faire des courses. Pour les courses, il y a beaucoup de magasins et donc
beaucoup de choix pour mes vêtements ou pour les ordinateurs (portables
ou pas). En plus, si on n\'a pas de voiture (comme moi), on peut
utiliser les transports en commun (comme le métro ou le bus). C\'est
trés vite et pratique, pas cher et tout le monde peut l\'utiliser.

Mais, il y a une autre facon (w/ cedilla) où on peut habiter à la
campagne. Ma famille a de la chance, car nous avons une maison à
Londres, cependant nous avons aussi un gîte au Pays de Galles, sur
l\'Ile d\'Anglesey. C\'est formidable car il a une belle vue sur la mer,
et il ne se trouve pas près d\'une plage, il se trouve presque sur une
plage! C\'est amusant y habiter car nous pouvons faire beaucoup de
sports nautiques que nous ne pouvons pas faire à Londres. En plus, nous
connaissons beaucoup de personnes au Pays de Galles, et nous ne
connaissons pas beaucoup de personnes à Londres. Cependant, il faut
aller loin pour un grand magasin ou pour aller à l\'Université.

Finalement, bien que j\'aime Rhosneigr, je préfère Londres car il y a
beaucoup de choses qui sont plus près de moi.
