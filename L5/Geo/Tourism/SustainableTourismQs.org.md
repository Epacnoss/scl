---
author: Jack Maguire
title: Sustainable Tourism Questions
---

# Define the Term Sustainable and write a description of the location of Sarawak and why it is a popular tourist destination.

Sustainable Tourism is tourism that by definition can keep on going for
the forseeable future, without any reason to stop other than things like
Natural Disasters or Financial Crashes. This includes things like no
single-use plastic, and using renewable energy, and keeping within the
natural carrying capacity.

Sarawak is a state of Malaysia, which is near the equator. It has a
tropical climate, and is hot all year round with wet and monsoon
seasons. The area is perfect for growing rubber and producing palm oil.
Sarawak borders with Brunei, Sabah and Indonesia. It is a place of
outstanding beauty, and has some international hotels. Average
temperature is 25^o^C, with rainfall of 200mm per month.

There are a multitude of reasons tourists go to Sarawak:

## Physical Reasons:

-   Lots of rare wildlife like Orangutans.
-   Beautiful Beaches with clear water.
-   The Satang islands have reefs.
-   Huge Cave Systems.
-   The rainforests attract adventurers.

## Human Reasons:

-   The Rainforest World Music Festival - it attracts over 20,000 people
    every year.
-   Diving, caving and a range of other activities.
-   There are many forts attracting historical and cultural tourists.
-   The Old state Mosque was built in 1852!
-   San Ching Tian Temple is the largest taoist temple in SE asia!

# What risks does tourism bring to Sarawak?

As with all beautiful tourist destinations, once enough people find out
about it, there will be a massive demand, and not all locals may be as
concerned with the environment, and their culture as they might be
concerned with money.

# What strategies have been used in Sarawak to ensure tourism is managed for sustainability.

-   The hotels have limited numbers.
-   They use greywater systems to reduce water usage.
-   They make sure to fish sustainably.
-   They might have agreements with local fishermen for fish.
-   They can use renewable power sources like Solar, or Tidal.

# Consider the information you have read - come up with a table of the goods and the bads. Also identify if they are economic, social or environmental.

## Good

-   A positive international image can lead to more investment. - Ec
-   Better Quality of Life for locals due to more money. - S
-   Local people may take a greater interest in looking after the
    environment for the tourists. - En
-   Improvements in Local Infrastructure paid by tourists paying tax. -
    Ec
-   More tax money for education/healthcare. - S
-   Lots of different types of employment if one type dies for some
    reason or another. - Ec

## Bad

-   Tourists may not respect local customs, may dress inappropriately or
    take insensitive photos. - S
-   Tourist development may use scarce resources like water or
    electricity. -S, En, Ec.
-   The culture of a country may become increasingly \'westernised\' to
    welcome tourists. -S
-   Pollution and waste may increase from tourist developments. -En

# Why is Ecotourism important to Sarawak?

Ecotourism is important to Sarawak because in order to preserve it\'s
heritage, it needs money, and better to accept tourism on their own
terms, than to have some corporate giant swallow it up.
