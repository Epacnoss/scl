---
author: Jack Maguire
title: Reasons for Growth in Tourism
---

# Pictures

-   (The Sun): Wanting a warmer climate, or a suntan.
-   (The Pyramid): Wanting to see Historical artifacts.
-   (The Animals): Wanting to see wild animals.
-   (The White-Water Rafting): A Desire to experience something outside
    of one\'s comfort zone.
-   (Natural Beauty): A Desire to see the beauty of nature.
-   (New York): A Desire to see a busy city ???

# TxtBook Q

During the last 50 years, tourist numbers have vastly increased due to a
number of factors, like:

-   Rapid air travel is much better than slow sea travel.
-   Flying is cheaper than ever.
-   Transport facilities are faster and more efficient.
-   Disposable Income has increased.
-   As people gain more disposable income, they demand more holidays.
-   The internet has helped to spread knowledge of other places.
-   Helps to cope with modern stresses.

# Graph Description

The graph shows the number of toursts received in 5 major areas (Africa,
the Middle East, the Americas, Asia/The Pacific, and Europe) from 1950
to 2010. Then, predictions are made from 2010 to 2030. At first, in
1950, barely any tourists leave their home area, and this is likely due
to a lack of affordable travel options. However, we can see an early
trend emerging with Europe being popular. Then, if we skip 30 years to
1980, we can see that our numbers have dramatically increased. Europe
received almost 200 million tourists that year, with the next biggest
area being the Americas. Unfortunately, the numbers are still too small
to properly measure with the exception of Europe. Then, if we skip
another 30 years to 2010, our tourist numbers have increased again. This
time, we can see a few downfalls as well, such as 9/11 in 2001, where
fewer people flew, and around 2008 the Stock Market Crash. If we keep at
2010 then, we have about 400 million per year going to europe, about 200
million per year to Asia and the Pacific (which is gaining over the
Americas), and just under that to the Americas. Africa and the Middle
East are both still too small to measure. Then, finally in 2020, we have
around 700 million tourists per year going to Europe, about 500 million
going to Asia and the Pacific, around 200 million to the Americas, and
maybe 100 million each to Africa and the Middle East.

# Multiplier Effect

? - The oil thing in deved vs undeved nations???

# Butler Graph

Txtbk needed.
