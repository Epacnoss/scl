---
title: Y Exercises
---

1.  Papa va au bureau demain?
2.  Vous irez au Pays de Galles en été?
3.  Tes parents sont à la maison?
4.  Est-ce qu'il irait en France s'il avait assez d'argent?
5.  Billy et Charlie sont dans le jardin?
6.  Elle est allée à l'école hier ?
7.  Tu étais dans la cuisine quand j'ai téléphoné?
8.  Les chats étaient dans ta chambre pendant la nuit?

```{=html}
<!-- -->
```
1.  Il y va. Il n\'y va pas
2.  Oui, nous y irons. Non, nous n\'y irons pas.
3.  Oui, ils y sont. Non, mes parents n\'y sont pas.
4.  Oui, il y irait.
5.  Ils y sont. Ils n\'y sont pas.
6.  Elle y est allée. Elle n\'y est pas allée.
7.  Je n\'y étais pas.
8.  Oui, ils y étaient. Non, ils n\'y étaient pas.
